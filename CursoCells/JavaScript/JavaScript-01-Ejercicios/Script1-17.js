
console.log('Problema 1');
function f ( x, y=2, z=7 ){
    return x + y + z;
}
console.log( f ( 5, undefined ));

console.log('--------------------- ---------');
console.log('Problema 2');
animal = 'kitty';
result = ( animal === 'kitty')? 'cute' : 'still nice';
console.log(result);

console.log('--------------------- ---------');
console.log('Problema 3');
var animal1 = 'kitty';
var result1 = '';
if (animal1 === 'kitty'){
    result1 = 'cute';
} else {
    result1 = 'still nice';
}
console.log(result1);

console.log('--------------------- ---------');
console.log('Problema 4');
var a = 0;
var str = 'condicion es ';
var b = '';
function funcion1 (){
    b = a === 0 ? (a = 1, str += ' true'): (a=2, str='false');
    console.log('a = '+a+' valor: '+str); 
}
funcion1();
console.log('--------------------- ---------');
console.log('Problema 5');
var a = 1;
var result3;
function funcion2 () { 
    result3 = a === 1 ? alert('Hey, esto es: '+a): 0;
}
//funcion2();

console.log('--------------------- ---------');
console.log('Problema 6');
 function fiternario () {
    if(a===1) alert('Hey, esto es 1_!'); else alert('Raro, ¿qué podría ser?');

    a === '1' ? alert('Hey, esto es 1!.'): alert('Raro, ¿qué podría ser?');

    }
//fiternario();

console.log('--------------------- ---------');
console.log('Problema 7');
var value = 2;
function casosUsos(){
    switch (value) {
        case 1:
            console.log('Yo podria siempre correr');
            break;
        case 2:
            console.log('Yo no podria correr siempre');
            break;
    }
}
casosUsos();
console.log('--------------------- ---------');
console.log('Problema 8');
var animales = 'Lion';
function casosAnimales(){
    switch (animales){
        case 'Dog':
            console.log('¡No correré desde animal! == "Perro"');
            break;
        case 'cat':
            console.log('¡No correré desde animal! == "Gato"');
            break;
        default:
            console.log('correre ya que el animal no coincide con ningun otro caso');
            break;
    }
}
casosAnimales();

console.log('--------------------- ---------');
console.log('Problema 9');
function john () {
    return 'John';
}
function jacob() {
    return 'Jacob';
}
function nombres( names ){
  switch (names) {
      case john():
      console.log('Correré si nombre ==== "John"');
      break;
      case 'Ja' + 'ne':
      console.log('Correré si nombre ==== "Jane"');
      break;
      case john() + ' ' + jacob()+' JingleHeimer Smith':
      console.log('Su nombre es igual a nombre también!');
      break;
  }  
}
nombres("John");
nombres("Jane");
nombres("John Jacob JingleHeimer Smith");
console.log('--------------------- ---------');
console.log('Problema 10');
var x = "d";
function casosUsos2(){
 switch(x){
     case "a":
     case "b":
     case "c":
         console.log("se seleccionó a, b o c");
        break;
     case "d":
             console.log("Solo se seleccionó d");
        break;
    default:
        console.log("no se encontro ningun caso");
        break;
 }
}
casosUsos2();
console.log('--------------------- ---------');
console.log('Problema 11');
var x1 = 5+7;
var x2 = 5 + "7";
var x3 = 5 - 7;
var x4 = 5 - "7";
var x5 = "5" - 7;
var x6 = 5 - "x";
console.log('Operacion x = 5 + 7: '+x1);
console.log('Operacion x = 5 + "7": '+ x2);
console.log('Operacion x = 5 - 7: '+x3);
console.log('Operacion x = 5 - "7": '+x4);
console.log('Operacion x = "5" - 7: '+x5);
console.log('Operacion x = 5 - "x": '+x6);
console.log('--------------------- ---------');
console.log('Problema 12');
function Problema12(){
var a = 'hello' || '';
var b = '' || [];
var c = '' || undefined;
var d = 1 || 5;
var e = 0 || {};
var f = 0 || '' || 5;
var g = '' || 'yay' || 'ya1';
console.log(''+a);
console.log(''+b);
console.log(''+c);
console.log(''+d);
console.log(''+e);
console.log(''+f);
console.log(''+g);
}
Problema12();
console.log('--------------------- ---------');
console.log('Problema 13');
function Problema13(){
    var a1 = 'hello' && '';
    var b1 = '' && [];
    var c1 = undefined && 0;
    var d1 = 1 && 5;
    var e1 = 0 && {};
    var f1 = 'hi' && [] && 'done';
    var g1 = 'bye' && undefined && 'adios';
    console.log(''+a1);
    console.log(''+b1);
    console.log(''+c1);
    console.log(''+d1);
    console.log(''+e1);
    console.log(''+f1);
    console.log(''+g1);
    }
    Problema13();
    console.log('--------------------- ---------');
console.log('Problema 13');
var foo = function (val) {
    return val || 'default';
}
console.log(foo('burger'));
console.log(foo(100));
console.log(foo([]));
console.log(foo(0));
console.log(foo(undefined));
console.log('--------------------- ---------');
console.log('Problema 14');
var age =19;
var height =5.6;
var status='';;
var isLegal = age >=18;
var tall = height >= 5.11;
var suitable = isLegal && tall;
var isRoyalty = status === 'royalty';
var specialCase = isRoyalty && hasInvitation;
var canEnterOurBar =suitable || specialCase;
console.log(isLegal);
console.log(tall);
console.log(suitable);
console.log(isRoyalty);
console.log(specialCase);
console.log(canEnterOurBar);
console.log('--------------------- ---------');
console.log('Problema 15');
for (let i = 0; i < 3; i++){
    if(i===1){
        continue;
    }
    console.log(i);
}
console.log('--------------------- ---------');
console.log('Problema 16');
var i = 0;
while (i < 3) {
    if (i===1){
        i=2;
        continue;
    }
    console.log(i);
    i++;
}
console.log('--------------------- ---------');
console.log('Problema 17');
for (var cont = 0; cont < 5; cont++){
    nextLoop2Iteration:
    for(var cont1 = 0; cont1 < 5;cont1 ++){

    if(cont == cont1) break  nextLoop2Iteration;

    console.log(cont, cont1);
    }
}
